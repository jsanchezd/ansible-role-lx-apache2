# Ansible Role Linux Apache2

## Description

This is an Ansible Role that you can add to any playbook in order to install a standard setup of Apache HTTP Server on Debian-based and RPM-based Linux distros.

Tested on CentOS 7 and Ubuntu 20.04.

## Dependencies

None

## Usage

In order to add this role on a playbook simply add it on the `roles` section or as an `include_role` directive inside one of the tasks files

#### Playbook example

```yaml
--- # Simple Apache2 playbook
- hosts: all
  become: yes
  become_method: sudo
  connection: ssh
  gather_facts: yes
  roles:
    - lx-apache2
  vars:
    mod_rewrite: yes
```

#### Tasks File example

```yaml
---
- name: Include Apache2 Role
  include_role:
    - name: lx-apache2 # or the name of the directory where you cloned this repository
```

### Variables

| Variable Name | Default Value | Possible Values            |
| ------------- | ------------- | -------------------------- |
| mod_rewrite   | yes           | y, yes, true, n, no, false |

